var map,
  activeSection = '',
  mapElem = $('#map');
function setFullHeight(){
  $('.js-fullheight').css('height', $(window).height());
}
function updateMap(newSection) {
  if (activeSection === newSection){
    return;
  }
  switch (newSection) {
    case 'akihabara':
      map.flyTo({
        bearing: 27,
        center: [139.7722703,35.7022077],
        zoom: 12
      });
      break;
    case 'asakusa':
      map.flyTo({
        bearing: -2,
        center: [139.794386,35.717031],
        zoom: 12
      });
      break;
    case 'odaiba':
      map.flyTo({
        bearing: 10,
        center: [139.775578,35.627442],
        zoom: 12
      });
      break;
    case 'shinjuku':
      map.flyTo({
        bearing: 0,
        center: [139.6916546,35.7015239],
        zoom: 13
      });
      break;
    case 'bunkio':
      map.flyTo({
        bearing: 25,
        center: [139.7273657,35.7177432],
        zoom: 13
      });
      break;
    case 'ginza':
      map.flyTo({
        bearing: -4,
        center: [139.7611172,35.6695672],
        zoom: 13
      });
      break;
  }
  activeSection = newSection;
}
function isOnScreen(elem){
  elem = $(elem);
  var win = $(window);
  var viewport = {
    top : win.scrollTop(),
    left : win.scrollLeft()
  };
  viewport.bottom = viewport.top + (win.height()/2);
  var bounds = elem.offset();
  bounds.bottom = bounds.top + elem.outerHeight();
  return !(viewport.bottom < bounds.top || viewport.top > bounds.bottom - (win.height()/2));
}
function highlightSection() {
  var newSection =
    isOnScreen('#akihabara') ? 'akihabara' :
    isOnScreen('#asakusa') ? 'asakusa' :
    isOnScreen('#odaiba') ? 'odaiba' :
    isOnScreen('#bunkio') ? 'bunkio' :
    isOnScreen('#ginza') ? 'ginza' :
    isOnScreen('#shinjuku') ? 'shinjuku' : '';
  updateMap(newSection);
}

function positionMap(){
  var scrollTop = $(window).scrollTop();
  //var windowHeight = $(window).innerHeight();
  var featuresHeight = $('#features').height();
  var windowWidth = $(window).innerWidth();

  if (windowWidth < 700) {
    if(scrollTop > $("#cabecera").outerHeight()){
        $('#map-container').removeClass('pin-bottom pin-left').addClass('sticky');
    }else{
        $('#map-container').removeClass('sticky pin-bottom').addClass('pin-left');
    }
  }else{
    if(scrollTop > $("#cabecera").outerHeight() + 40){
        $('#map-container').removeClass('pin-bottom pin-left').addClass('sticky');
    }else{
        $('#map-container').removeClass('sticky pin-bottom').addClass('pin-left');
    }
  }
  if(scrollTop > ($("#cabecera").outerHeight() + featuresHeight - $(window).height())){
      $('#map-container').removeClass('sticky').addClass('pin-bottom');
  }
}

function init() {
  mapElem.addClass('loading');
  setFullHeight();
  positionMap();
  mapboxgl.accessToken = 'pk.eyJ1IjoiYmFsZGJveSIsImEiOiJhMzBzeklzIn0.buJ1PP9-a9JkqNWGHW-H0g';
  map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/baldboy/cimhus8ss0060d0mcowu7vuhz',
    center: [139.750721,35.652063],
    zoom: 12,
    maxZoom: 14,
    minZoom: 11
  });
  map.on('load', function() {
    $(window).resize(function () {
      setFullHeight();
    });

    $(window).scroll(function() {
      positionMap();
      highlightSection();
    });
    mapElem.removeClass('loading');

    map.addSource('yamanote_line', { type: 'geojson', data: 'http://localhost:4000/js/Yamanote_Line.geojson'});
    map.addLayer({
      "id": "route",
      "type": "line",
      "source": "yamanote_line",
      "layout": {
        "line-join": "round",
        "line-cap": "round"
      },
      "paint": {
          "line-color": "#888",
          "line-width": 8
      }  
    });

  });

  var metroTokyo = {
    type: 'FeatureCollection',
    features: [{
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.701646,35.658759]
      },
      properties: {
        title: 'Shibuya',
        description: 'G01 渋谷駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.712532,35.665155]
      },
      properties: {
        title: 'Omotesandō',
        description: 'G02 表参道駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.717723,35.670454]
      },
      properties: {
        title: 'Gaiemmae',
        description: 'G03 外苑前駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.724008,35.672785]
      },
      properties: {
        title: 'Aoyama-Itchōme',
        description: 'G04 青山一丁目駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.737256,35.676746]
      },
      properties: {
        title: 'Akasaka-Mitsuke',
        description: 'G05 赤坂見附駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.741430,35.671912]
      },
      properties: {
        title: 'Tameike-Sannō',
        description: 'G06 溜池山王駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7500728,35.6701288]
      },
      properties: {
        title: 'Toranomon',
        description: 'G07 虎ノ門駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.758561,35.667379]
      },
      properties: {
        title: 'Shimbashi',
        description: 'G08 新橋駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.765023,35.671214]
      },
      properties: {
        title: 'Ginza',
        description: 'G09 銀座駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7700445,35.6766786]
      },
      properties: {
        title: 'Kyōbashi',
        description: 'G10 京橋駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7737091,35.6825332]
      },
      properties: {
        title: 'Nihombashi',
        description: 'G11 日本橋駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.774596,35.684611]
      },
      properties: {
        title: 'Mitsukoshimae',
        description: 'G12 三越前駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.771432,35.692404]
      },
      properties: {
        title: 'Kanda',
        description: 'G13 神田駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.771733,35.7029622]
      },
      properties: {
        title: 'Suehirochō',
        description: 'G14 末広町駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7730855,35.7078922]
      },
      properties: {
        title: 'Ueno-Hirokōji',
        description: 'G15 上野広小路駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.777983,35.712191]
      },
      properties: {
        title: 'Ueno',
        description: 'G16 上野駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.782207,35.7113758]
      },
      properties: {
        title: 'Inarichō',
        description: 'G17 稲荷町駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7907552,35.7098044]
      },
      properties: {
        title: 'Tawaramachi',
        description: 'G18 田原町駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7976753,35.7107115]
      },
      properties: {
        title: 'Asakusa',
        description: 'G19 浅草駅',
        line: 'Ginza Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.619972,35.704287]
      },
      properties: {
        title: 'Ogikubo',
        description: 'M01 荻窪駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.635549,35.699435]
      },
      properties: {
        title: 'Minami-Asagaya',
        description: 'M02 南阿佐ケ谷駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.648296,35.697813]
      },
      properties: {
        title: 'Shin-Kōenji',
        description: 'M03 新高円寺駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.658267,35.697953]
      },
      properties: {
        title: 'Higashi-Kōenji',
        description: 'M04	東高円寺駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.682826,35.696988]
      },
      properties: {
        title: 'Nakano-Sakaue',
        description: 'M06	中野坂上駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.692920,35.694382]
      },
      properties: {
        title: 'Nishi-Shinjuku',
        description: 'M07	西新宿駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.699342,35.692141]
      },
      properties: {
        title: 'Shinjuku',
        description: 'M08	新宿駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.704837,35.690874]
      },
      properties: {
        title: 'Shinjuku-sanchōme',
        description: 'M09	新宿三丁目駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.710853,35.688546]
      },
      properties: {
        title: 'Shinjuku-gyoemmae',
        description: 'M10	新宿御苑前駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.720537,35.687987]
      },
      properties: {
        title: 'Yotsuya-sanchōme',
        description: 'M11	四谷三丁目駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.729468,35.685456]
      },
      properties: {
        title: 'Yotsuya',
        description: 'M12	四ツ谷駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.737256,35.676746]
      },
      properties: {
        title: 'Akasaka-mitsuke',
        description: 'M13	赤坂見附駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.743273,35.673711]
      },
      properties: {
        title: 'Kokkai-gijidō-mae',
        description: 'M14	国会議事堂前駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751553,35.674732]
      },
      properties: {
        title: 'Kasumigaseki',
        description: 'M15	霞ケ関駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.762857,35.672795]
      },
      properties: {
        title: 'Ginza',
        description: 'M16	銀座駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.764879,35.681794]
      },
      properties: {
        title: 'Tokyo',
        description: 'M17	東京駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.766182,35.686513]
      },
      properties: {
        title: 'Ōtemachi',
        description: 'M18	大手町駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7674602,35.69496]
      },
      properties: {
        title: 'Awajichō',
        description: 'M19	淡路町駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7642972,35.7004917]
      },
      properties: {
        title: 'Ochanomizu',
        description: 'M20	御茶ノ水駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.760087,35.706570]
      },
      properties: {
        title: 'Hongō-sanchōme',
        description: 'M21	本郷三丁目駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751856,35.707339]
      },
      properties: {
        title: 'Kōrakuen',
        description: 'M22	後楽園駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.736795,35.717380]
      },
      properties: {
        title: 'Myōgadani',
        description: 'M23	茗荷谷駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.729922,35.725797]
      },
      properties: {
        title: 'Shin-ōtsuka',
        description: 'M24	新大塚駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.710696,35.730517]
      },
      properties: {
        title: 'Ikebukuro',
        description: 'M25	池袋駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.657623,35.683497]
      },
      properties: {
        title: 'Hōnanchō',
        description: 'm03	方南町駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.667004,35.690542]
      },
      properties: {
        title: 'Nakano-fujimichō',
        description: 'm04	中野富士見町駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.674180,35.692502]
      },
      properties: {
        title: 'Nakano-shimbashi',
        description: 'm05	中野新橋駅',
        line: 'Marunouchi Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.6990956,35.6442877]
      },
      properties: {
        title: 'Naka-Meguro',
        description: 'H01 中目黒駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.709470,35.647854]
      },
      properties: {
        title: 'Ebisu',
        description: 'H02 恵比寿駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7222456,35.6518542]
      },
      properties: {
        title: 'Hiroo',
        description: 'H03 広尾駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.732291,35.663074]
      },
      properties: {
        title: 'Roppongi',
        description: 'H04 六本木駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.744765,35.662682]
      },
      properties: {
        title: 'Kamiyachō',
        description: 'H05 神谷町駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751553,35.674732]
      },
      properties: {
        title: 'Kasumigaseki',
        description: 'H06 霞ケ関駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.759596,35.674975]
      },
      properties: {
        title: 'Hibiya',
        description: 'H07 日比谷駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.762857,35.672795]
      },
      properties: {
        title: 'Ginza',
        description: 'H08 銀座駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.766989,35.669715]
      },
      properties: {
        title: 'Higashi-ginza',
        description: 'H09 東銀座駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.772398,35.667964]
      },
      properties: {
        title: 'Tsukiji',
        description: 'H10 築地駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.776877,35.674627]
      },
      properties: {
        title: 'Hatchōbori',
        description: 'H11 八丁堀駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.780164,35.679942]
      },
      properties: {
        title: 'Kayabachō',
        description: 'H12 茅場町駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.782266,35.686340]
      },
      properties: {
        title: 'Ningyōchō',
        description: 'H13 人形町駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.778491,35.690757]
      },
      properties: {
        title: 'Kodemmachō',
        description: 'H14 小伝馬町駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7762036,35.7066284]
      },
      properties: {
        title: 'Naka-Okachimachi',
        description: 'H16 仲御徒町駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.777983,35.712191]
      },
      properties: {
        title: 'Ueno',
        description: 'H17 上野駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.784446,35.720564]
      },
      properties: {
        title: 'Iriya',
        description: 'H18 入谷駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.791203,35.729415]
      },
      properties: {
        title: 'Minowa',
        description: 'H19 三ノ輪駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.799457,35.733218]
      },
      properties: {
        title: 'Minami-Senju',
        description: 'H20 南千住駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.805820,35.750427]
      },
      properties: {
        title: 'Kita-Senju',
        description: 'H21 北千住駅',
        line: 'Hibiya Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.666065,35.705763]
      },
      properties: {
        title: 'Nakano',
        description: 'T01 中野駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.686116,35.710699]
      },
      properties: {
        title: 'Ochiai',
        description: 'T02 落合駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.704058,35.713744]
      },
      properties: {
        title: 'Takadanobaba',
        description: 'T03 高田馬場駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7209695,35.7058329]
      },
      properties: {
        title: 'Waseda',
        description: 'T04 早稲田駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.734526,35.703850]
      },
      properties: {
        title: 'Kagurazaka',
        description: 'T05 神楽坂駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.744442,35.702629]
      },
      properties: {
        title: 'Iidabashi',
        description: 'T06 飯田橋駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751415,35.695495]
      },
      properties: {
        title: 'Kudanshita',
        description: 'T07 九段下駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.758319,35.690246]
      },
      properties: {
        title: 'Takebashi',
        description: 'T08 竹橋駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.763156,35.685256]
      },
      properties: {
        title: 'Ōtemachi',
        description: 'T09 大手町駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7737091,35.6825332]
      },
      properties: {
        title: 'Nihombashi',
        description: 'T10 日本橋駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.780164,35.679942]
      },
      properties: {
        title: 'Kayabachō',
        description: 'T11 茅場町駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.794901,35.672379]
      },
      properties: {
        title: 'Monzen-Nakachō',
        description: 'T12 門前仲町駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.8064736,35.6694005]
      },
      properties: {
        title: 'Kiba',
        description: 'T13 木場駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.818853,35.669653]
      },
      properties: {
        title: 'Tōyōchō',
        description: 'T14 東陽町駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.831675,35.668332]
      },
      properties: {
        title: 'Minami-Sunamachi',
        description: 'T15 南砂町駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.859178,35.664591]
      },
      properties: {
        title: 'Nishi-Kasai',
        description: 'T16 西葛西駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.872926,35.663596]
      },
      properties: {
        title: 'Kasai',
        description: 'T17 葛西駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.893190,35.665929]
      },
      properties: {
        title: 'Kasai',
        description: 'T18 浦安駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.902247,35.672707]
      },
      properties: {
        title: 'Minami-Gyōtoku',
        description: 'T19 南行徳駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.914158,35.682595]
      },
      properties: {
        title: 'Gyōtoku',
        description: 'T20 行徳駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.924366,35.691049]
      },
      properties: {
        title: 'Myōden',
        description: 'T21 妙典駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.941671,35.703117]
      },
      properties: {
        title: 'Baraki-Nakayama	',
        description: 'T22 原木中山駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.959003,35.707174]
      },
      properties: {
        title: 'Nishi-Funabashi',
        description: 'T23 西船橋駅',
        line: 'Tozai Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.679806,35.668978]
      },
      properties: {
        title: 'Yoyogi-Uehara',
        description: 'C01 代々木上原駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.689769,35.669087]
      },
      properties: {
        title: 'Yoyogi-Kōen',
        description: 'C02 代々木公園駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.705414,35.668493]
      },
      properties: {
        title: 'Meiji-Jingūmae (Harajuku)',
        description: 'C03 明治神宮前〈原宿〉駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.712532,35.665155]
      },
      properties: {
        title: 'Omotesandō',
        description: 'C04 表参道駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.726194,35.666535]
      },
      properties: {
        title: 'Nogizaka',
        description: 'C05 乃木坂駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.736411,35.672231]
      },
      properties: {
        title: 'Akasaka',
        description: 'C06 赤坂駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.743273,35.673711]
      },
      properties: {
        title: 'Kokkai-Gijidō-mae',
        description: 'C07 国会議事堂前駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751650,35.672457]
      },
      properties: {
        title: 'Kasumigaseki',
        description: 'C08 霞ケ関駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.759596,35.674975]
      },
      properties: {
        title: 'Hibiya',
        description: 'C09 日比谷駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.761818,35.680708]
      },
      properties: {
        title: 'Nijūbashimae',
        description: 'C10 二重橋前駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.763156,35.685256]
      },
      properties: {
        title: 'Ōtemachi',
        description: 'C11 大手町駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.765435,35.696915]
      },
      properties: {
        title: 'Shin-Ochanomizu',
        description: 'C12 新御茶ノ水駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.769968,35.706844]
      },
      properties: {
        title: 'Yushima',
        description: 'C13 湯島駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.765762,35.717354]
      },
      properties: {
        title: 'Nezu',
        description: 'C14 根津駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.763328,35.725784]
      },
      properties: {
        title: 'Sendagi',
        description: 'C15 千駄木駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.766607,35.732256]
      },
      properties: {
        title: 'Nishi-Nippori',
        description: 'C16 西日暮里駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.780580,35.742862]
      },
      properties: {
        title: 'Machiya',
        description: 'C17 町屋駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.804590,35.749749]
      },
      properties: {
        title: 'Kita-Senju',
        description: 'C18 北千住駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.825157,35.762118]
      },
      properties: {
        title: 'Ayase',
        description: 'C19 綾瀬駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.832140,35.776984]
      },
      properties: {
        title: 'Kita-Ayase',
        description: 'C20 北綾瀬駅',
        line: 'Chiyoda Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.612439,35.788519]
      },
      properties: {
        title: 'Wakōshi',
        description: 'Y01 和光市駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.631250,35.776703]
      },
      properties: {
        title: 'Chikatetsu-Narimasu',
        description: 'Y02 地下鉄成増駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.644076,35.769949]
      },
      properties: {
        title: 'Chikatetsu-Akatsuka',
        description: 'Y03 地下鉄赤塚駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.654326,35.757542]
      },
      properties: {
        title: 'Heiwadai',
        description: 'Y04 平和台駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.665503,35.749601]
      },
      properties: {
        title: 'Hikawadai',
        description: 'Y05 氷川台駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.679747,35.743430]
      },
      properties: {
        title: 'Kotake-Mukaihara',
        description: 'Y06 小竹向原駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.689456,35.738190]
      },
      properties: {
        title: 'Senkawa',
        description: 'Y07 千川駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.698411,35.733300]
      },
      properties: {
        title: 'Kanamechō',
        description: 'Y08 要町駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.710696,35.730517]
      },
      properties: {
        title: 'Ikebukuro',
        description: 'Y09 池袋駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.718980,35.726031]
      },
      properties: {
        title: 'Higashi-Ikebukuro',
        description: 'Y10 東池袋駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.726950,35.720104]
      },
      properties: {
        title: 'Gokokuji',
        description: 'Y11 護国寺駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.734116,35.709301]
      },
      properties: {
        title: 'Edogawabashi',
        description: 'Y12 江戸川橋駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.744442,35.702629]
      },
      properties: {
        title: 'Iidabashi',
        description: 'Y13 飯田橋駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.735937,35.691075]
      },
      properties: {
        title: 'Ichigaya',
        description: 'Y14 市ケ谷駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.737262,35.684985]
      },
      properties: {
        title: 'Kōjimachi',
        description: 'Y15 麹町駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.739996,35.678533]
      },
      properties: {
        title: 'Nagatachō',
        description: 'Y16 永田町駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751387,35.677339]
      },
      properties: {
        title: 'Sakuradamon',
        description: 'Y17 桜田門駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.764074,35.675535]
      },
      properties: {
        title: 'Yūrakuchō',
        description: 'Y18 有楽町駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7669788,35.6743941]
      },
      properties: {
        title: 'Ginza-itchōme',
        description: 'Y19 銀座一丁目駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.773631,35.670545]
      },
      properties: {
        title: 'Shintomichō',
        description: 'Y20 新富町駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.7848235,35.6643919]
      },
      properties: {
        title: 'Tsukishima',
        description: 'Y21 月島駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.796565,35.654606]
      },
      properties: {
        title: 'Toyosu',
        description: 'Y22 豊洲駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.810243,35.645800]
      },
      properties: {
        title: 'Tatsumi',
        description: 'Y23 辰巳駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.826627,35.646029]
      },
      properties: {
        title: 'Shin-Kiba',
        description: 'Y24 新木場駅',
        line: 'Yurakucho Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.701646,35.658759]
      },
      properties: {
        title: 'Shibuya',
        description: 'Z01 渋谷駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.712532,35.665155]
      },
      properties: {
        title: 'Omotesandō',
        description: 'Z02 表参道駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.724008,35.672785]
      },
      properties: {
        title: 'Aoyama-itchōme',
        description: 'Z03 青山一丁目駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.739996,35.678533]
      },
      properties: {
        title: 'Nagatachō',
        description: 'Z04 永田町駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.741606,35.685401]
      },
      properties: {
        title: 'Hanzomon',
        description: 'Z05 半蔵門駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751415,35.695495]
      },
      properties: {
        title: 'Kudanshita',
        description: 'Z06 九段下駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.758080,35.695929]
      },
      properties: {
        title: 'Jimbōchō',
        description: 'Z07 神保町駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.763661,35.686855]
      },
      properties: {
        title: 'Ōtemachi',
        description: 'Z08 大手町駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.774596,35.684611]
      },
      properties: {
        title: 'Mitsukoshimae',
        description: 'Z09 三越前駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.785192,35.683041]
      },
      properties: {
        title: 'Suitengūmae',
        description: 'Z10 水天宮前駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.798798,35.682044]
      },
      properties: {
        title: 'Kiyosumi-shirakawa',
        description: 'Z11 清澄白河駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.815722,35.689005]
      },
      properties: {
        title: 'Sumiyoshi',
        description: 'Z12 住吉駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.814950,35.696759]
      },
      properties: {
        title: 'Kinshichō',
        description: 'Z13 錦糸町駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.813401,35.710191]
      },
      properties: {
        title: 'Oshiage	',
        description: 'Z14 押上〈スカイツリー前〉駅',
        line: 'Hanzomon Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.715602,35.63291]
      },
      properties: {
        title: 'Meguro',
        description: 'N01 目黒駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.725537,35.637618]
      },
      properties: {
        title: 'Shirokanedai',
        description: 'N02 白金台駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.734184,35.642902]
      },
      properties: {
        title: 'Shirokane-Takanawa',
        description: 'N03 白金高輪駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.738916,35.665280]
      },
      properties: {
        title: 'Azabu-Jūban',
        description: 'N05 六本木一丁目駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.741430,35.671912]
      },
      properties: {
        title: 'Tameike-Sannō',
        description: 'N06 溜池山王駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.739996,35.678533]
      },
      properties: {
        title: 'Nagatachō',
        description: 'N07 永田町駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.729496,35.685884]
      },
      properties: {
        title: 'Yotsuya',
        description: 'N08 四ツ谷駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.736555,35.693099]
      },
      properties: {
        title: 'Ichigaya',
        description: 'N09 市ケ谷駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.745665,35.703102]
      },
      properties: {
        title: 'Iidabashi',
        description: 'N10 飯田橋駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.751844,35.708511]
      },
      properties: {
        title: 'Kōrakuen',
        description: 'N11 後楽園駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.758077,35.717702]
      },
      properties: {
        title: 'Tōdaimae',
        description: 'N12 東大前駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.753786,35.724635]
      },
      properties: {
        title: 'Hon-Komagome',
        description: 'N13 本駒込駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.746791,35.736018]
      },
      properties: {
        title: 'Komagome',
        description: 'N14 駒込駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.742424,35.745833]
      },
      properties: {
        title: 'Nishigahara',
        description: 'N15 西ケ原駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.737575,35.753608]
      },
      properties: {
        title: 'Ōji',
        description: 'N16 王子駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.735611,35.765779]
      },
      properties: {
        title: 'Ōji-Kamiya',
        description: 'N17 王子神谷駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.732511,35.778097]
      },
      properties: {
        title: 'Shimo',
        description: 'N18 志茂駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.722511,35.783363]
      },
      properties: {
        title: 'Akabane-Iwabuchi',
        description: 'N19 赤羽岩淵駅',
        line: 'Namboku Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.612439,35.788519]
      },
      properties: {
        title: 'Wakōshi',
        description: 'F01 和光市駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.631250,35.776703]
      },
      properties: {
        title: 'Chikatetsu-Narimasu',
        description: 'F02 地下鉄成増駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.644076,35.769949]
      },
      properties: {
        title: 'Chikatetsu-Akatsuka',
        description: 'F03 地下鉄赤塚駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.654326,35.757542]
      },
      properties: {
        title: 'Heiwadai',
        description: 'F04 平和台駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.665503,35.749601]
      },
      properties: {
        title: 'Hikawadai',
        description: 'F05 氷川台駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.679747,35.743430]
      },
      properties: {
        title: 'Kotake-Mukaihara',
        description: 'F06 小竹向原駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.689456,35.738190]
      },
      properties: {
        title: 'Senkawa',
        description: 'F07 千川駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.698411,35.733300]
      },
      properties: {
        title: 'Kanamechō',
        description: 'F08 要町駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.710696,35.730517]
      },
      properties: {
        title: 'Ikebukuro',
        description: 'F09 池袋駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.718926,35.726059]
      },
      properties: {
        title: 'Zōshigaya',
        description: 'F10 雑司が谷駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.709014,35.707835]
      },
      properties: {
        title: 'Nishi-Waseda',
        description: 'F11 西早稲田駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.707437,35.697957]
      },
      properties: {
        title: 'Higashi-Shinjuku',
        description: 'F12 東新宿駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.704837,35.690874]
      },
      properties: {
        title: 'Shinjuku-Sanchōme',
        description: 'F13 新宿三丁目駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.705107,35.678917]
      },
      properties: {
        title: 'Kitasandō',
        description: 'F14 北参道駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.705414,35.668493]
      },
      properties: {
        title: 'Meiji-Jingūmae',
        description: 'F15 明治神宮前〈原宿〉駅',
        line: 'Fukutoshin Line'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [139.701646,35.658759]
      },
      properties: {
        title: 'Shibuya',
        description: 'F16 渋谷駅',
        line: 'Fukutoshin Line'
      }
    }]
  };
  metroTokyo.features.forEach(function(marker) {
    var el = document.createElement('div');
    el.className = 'marker marker-' + marker.properties.line.replace(/\s+/g, '');
    new mapboxgl.Marker(el)
    .setLngLat(marker.geometry.coordinates)
    new mapboxgl.Marker(el)
    .setLngLat(marker.geometry.coordinates)
    .setPopup(new mapboxgl.Popup({ offset: 5 })
    .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
    .addTo(map);
  });


  






  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
}


init();
var $container = $('#container');
$container.imagesLoaded(function(){
    $container.masonry({
        itemSelector: '.item',
        columnWidth: function(containerWidth){
            return containerWidth / 12;
        }
    });
    $('.item img').addClass('not-loaded');
    $('.item img.not-loaded').lazyload({
        load: function() {
            // Disable trigger on this image
            $(this).addClass('loaded');
            $(this).removeClass("not-loaded");
            $container.masonry('reload');
        }
    });
    $('.item img.not-loaded').trigger('scroll');
});
lightbox.option();